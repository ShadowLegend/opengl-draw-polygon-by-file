
cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(polygon-drawer)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)
#set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(OpenGL_GL_PREFERENCE "GLVND")

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)

include_directories(
    include
    lib
    ${GLUT_INCLUDE_DIR}
    ${OPENGL_INCLUDE_DIR}
)

add_executable(
    polygon-drawer
    main.cpp
)

target_link_libraries(
    polygon-drawer
    ${OPENGL_LIBRARIES}
    ${GLUT_LIBRARY}
    ${SOIL2_LIBRARY}
)
