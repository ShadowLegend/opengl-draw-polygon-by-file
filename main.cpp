
#include <iostream>
#include <fstream>
#include <list>

#define WIDTH 960
#define HEIGHT 630

#if __APPLE__
#include <OpenGL/gl.h>
#elif __WINDOW__
#include <GL/gl.h>
#include <GL/glut.h>
#elif __unix__
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#endif

struct Coordinate {
  Coordinate(const int x, const int y) : x(x), y(y) {}

  int x;
  int y;
};

std::list<Coordinate> coordinates = {};
std::string line;
int time_value = 0;

void render();
void drawPolygon();
void timerFunc(int value);

void windowOnChange(int width, int height);
void readFile(const char *);

int main(int args_len, char ** args_context) {
  if (args_len < 2) {
    std::cout << "Usage: ./polygon-drawer <valid-file-path>" << std::endl;
  } else {
    readFile(args_context[1]);

    glutInit(&args_len, args_context);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutInitWindowPosition(100, 50);
    glutCreateWindow("Polygon Drawer");

    glEnable(GL_DEPTH_TEST);

    glutDisplayFunc(render);
    glutReshapeFunc(windowOnChange);

    glutTimerFunc(500, timerFunc, time_value);

    glutMainLoop();
  }

  return 0;
};

void render() {
  glClearColor(.2, .3, .5, .8);
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  drawPolygon();

  glFlush();
  glutSwapBuffers();
};

void timerFunc(int value) {
  glutPostRedisplay();

  if (coordinates.size() == time_value) {
    time_value = 0;
  } else {
    ++time_value;
  }

  glutTimerFunc(500, timerFunc, time_value);
};

void drawPolygon() {
  glPushMatrix();
    glTranslated(0, 0, -500);

    glBegin(GL_LINE_STRIP);
      glColor3d(1.0, 1.0, 1.0);

      int index = 0;

      for (std::list<Coordinate>::iterator it = coordinates.begin(); it != coordinates.end(); ++it) {
        if (index == time_value) {
          break;
        }

        glVertex2i(it->x, it->y);
        ++index;
      }
    glEnd();
  glPopMatrix();
};

void windowOnChange(int width, int height) {
  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, static_cast < GLfloat > ( width ) / static_cast < GLfloat > ( height ), .01, 1200.0);
  glMatrixMode(GL_MODELVIEW);
}

void readFile(const char *path) {
  std::ifstream file(path);
  int x = 0;
  int y = 0;
  int index = 0;

  while (std::getline(file, line)) {
    index = line.find(',');

    x = std::stoi(line.substr(0, index));
    y = std::stoi(line.substr(index + 1));

    Coordinate coordinate(x, y);

    coordinates.push_back(coordinate);
  }
};
